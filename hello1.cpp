///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello C++
//
// Usage:  Print hello world, with "using namespace std", must not qualify any standard library functions/methods/objects with std::
//
// Result:
//   Hello World!
//
// @file hello1.cpp
// @author Dane Takenaka <daneht@hawaii.edu>
// @date   2/13/2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

using namespace std;

int main() {

   cout << "Hello World!" << endl;
   

   
}
