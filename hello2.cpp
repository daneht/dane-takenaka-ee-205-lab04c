///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello C++
//
// Usage:  Print hello world, without "using namespace std", must qualify any standard library functions/methods/objects with std::
//
// Result:
//   Hello World!
//
//
// @author Dane Takenaka <daneht@hawaii.edu>
// @date   2/13/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() {

   std::cout << "Hello World!" << std::endl;
   return 0;
}
